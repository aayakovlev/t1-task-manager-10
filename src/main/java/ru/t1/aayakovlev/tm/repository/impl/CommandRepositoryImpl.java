package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.repository.CommandRepository;

public final class CommandRepositoryImpl implements CommandRepository {

    private static final Command ABOUT = new Command(
            CommandConstant.ABOUT, ArgumentConstant.ABOUT,
            "Show developer info."
    );

    private static final Command INFO = new Command(
            CommandConstant.INFO, ArgumentConstant.INFO,
            "Show hardware info."
    );

    private static final Command HELP = new Command(
            CommandConstant.HELP, ArgumentConstant.HELP,
            "Show arguments description."
    );

    private static final Command VERSION = new Command(
            CommandConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command EXIT = new Command(
            CommandConstant.EXIT, null,
            "Exit program.");

    private static final Command PROJECT_LIST = new Command(
            CommandConstant.PROJECT_LIST, null,
            "Show project list.");

    private static final Command PROJECT_CREATE = new Command(
            CommandConstant.PROJECT_CREATE, null,
            "Create new project.");

    private static final Command PROJECT_CLEAR = new Command(
            CommandConstant.PROJECT_CLEAR, null,
            "Clear project list.");

    private static final Command TASK_LIST = new Command(
            CommandConstant.TASK_LIST, null,
            "Show task list.");

    private static final Command TASK_CREATE = new Command(
            CommandConstant.TASK_CREATE, null,
            "Create new task.");

    private static final Command TASK_CLEAR = new Command(
            CommandConstant.TASK_CLEAR, null,
            "Clear task list.");

    private static final Command[] COMMANDS = new Command[]{
            ABOUT, INFO, HELP, VERSION, EXIT,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
