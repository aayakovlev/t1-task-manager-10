package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Task;

public interface TaskRepository extends BaseRepository<Task> {
}
