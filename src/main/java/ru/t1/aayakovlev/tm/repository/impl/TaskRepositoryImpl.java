package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepositoryImpl implements TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task save(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

}
