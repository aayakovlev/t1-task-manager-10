package ru.t1.aayakovlev.tm.repository;

import java.util.List;

public interface BaseRepository<T> {

    List<T> findAll();

    T save(final T e);

    void deleteAll();

}
