package ru.t1.aayakovlev.tm.component;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.controller.CommandController;
import ru.t1.aayakovlev.tm.controller.impl.CommandControllerImpl;
import ru.t1.aayakovlev.tm.controller.impl.ProjectControllerImpl;
import ru.t1.aayakovlev.tm.controller.impl.TaskControllerImpl;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.CommandService;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.TaskService;
import ru.t1.aayakovlev.tm.service.impl.CommandServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ProjectServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.TaskServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.EMPTY_ARRAY_SIZE;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class Bootstrap {

    private static final CommandRepository commandRepository = new CommandRepositoryImpl();

    private static final CommandService commandService = new CommandServiceImpl(commandRepository);

    private static final CommandController commandController = new CommandControllerImpl(commandService);

    private static final TaskRepository taskRepository = new TaskRepositoryImpl();

    private static final TaskService taskService = new TaskServiceImpl(taskRepository);

    private static final BaseController taskController = new TaskControllerImpl(taskService);

    private static final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    private static final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    private static final BaseController projectController = new ProjectControllerImpl(projectService);

    public void run(final String[] args) {
        processArguments(args);
        processCommand();
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return;

        final String argument = arguments[FIRST_ARRAY_ELEMENT];
        processArgument(argument);
    }

    private void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;

        switch (argument) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand() {
        System.out.println("**Welcome to Task Manager!**");
        String command;

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            command = nextLine();

            switch (command) {
                case CommandConstant.ABOUT:
                    commandController.showAbout();
                    break;
                case CommandConstant.VERSION:
                    commandController.showVersion();
                    break;
                case CommandConstant.HELP:
                    commandController.showHelp();
                    break;
                case CommandConstant.INFO:
                    commandController.showInfo();
                    break;
                case CommandConstant.PROJECT_LIST:
                    projectController.showAll();
                    break;
                case CommandConstant.PROJECT_CREATE:
                    projectController.create();
                    break;
                case CommandConstant.PROJECT_CLEAR:
                    projectController.clear();
                    break;
                case CommandConstant.TASK_LIST:
                    taskController.showAll();
                    break;
                case CommandConstant.TASK_CREATE:
                    taskController.create();
                    break;
                case CommandConstant.TASK_CLEAR:
                    taskController.clear();
                    break;
                case CommandConstant.EXIT:
                    commandController.showExit();
                default:
                    commandController.showCommandError();
                    break;
            }
        }
    }

}
