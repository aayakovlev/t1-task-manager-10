package ru.t1.aayakovlev.tm.constant;

public final class CommandConstant {

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String VERSION = "version";

    public final static String EXIT = "exit";

    public final static String INFO = "info";

    public final static String PROJECT_LIST = "project-list";

    public final static String PROJECT_CLEAR = "project-clear";

    public final static String PROJECT_CREATE = "project-create";

    public final static String TASK_LIST = "task-list";

    public final static String TASK_CLEAR = "task-clear";

    public final static String TASK_CREATE = "task-create";

    private CommandConstant() {
    }

}
