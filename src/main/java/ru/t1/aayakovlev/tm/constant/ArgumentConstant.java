package ru.t1.aayakovlev.tm.constant;

public final class ArgumentConstant {

    public final static String HELP = "-h";

    public final static String ABOUT = "-a";

    public final static String VERSION = "-v";

    public final static String INFO = "-i";

    private ArgumentConstant() {
    }

}
