package ru.t1.aayakovlev.tm.controller;

public interface CommandController {

    void showAbout();

    void showVersion();

    void showHelp();

    void showInfo();

    void showExit();

    void showArgumentError();

    void showCommandError();

}
