package ru.t1.aayakovlev.tm.controller;

public interface BaseController {

    void showAll();

    void create();

    void clear();

}
