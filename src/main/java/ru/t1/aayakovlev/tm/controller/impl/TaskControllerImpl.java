package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskControllerImpl implements BaseController {

    private final TaskService service;

    public TaskControllerImpl(TaskService service) {
        this.service = service;
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL]");
        final List<Task> tasks = service.findAll();
        int index = 1;

        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        final String name = nextLine();

        System.out.print("Enter description: ");
        final String description = nextLine();

        final Task task = service.create(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR TASKS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

}
