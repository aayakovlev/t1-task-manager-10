package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectControllerImpl implements BaseController {

    private final ProjectService service;

    public ProjectControllerImpl(ProjectService service) {
        this.service = service;
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL]");
        final List<Project> projects = service.findAll();
        int index = 1;

        for (final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        final String name = nextLine();

        System.out.print("Enter description: ");
        final String description = nextLine();

        final Project project = service.create(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR]");
        service.deleteAll();
        System.out.println("[OK]");
    }

}
