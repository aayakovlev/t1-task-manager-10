package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

public final class TaskServiceImpl implements TaskService {

    private final TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty())
            return null;
        if (description == null || description.isEmpty())
            return null;

        final Task task = new Task(name, description);
        save(task);
        return task;
    }

    @Override
    public void save(final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

}
