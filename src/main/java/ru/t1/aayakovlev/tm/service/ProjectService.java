package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectService extends BaseService<Project> {

    Project create(final String name, final String description);

}
