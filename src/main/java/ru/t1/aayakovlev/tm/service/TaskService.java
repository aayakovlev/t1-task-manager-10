package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Task;

public interface TaskService extends BaseService<Task> {

    Task create(final String name, final String description);

}
