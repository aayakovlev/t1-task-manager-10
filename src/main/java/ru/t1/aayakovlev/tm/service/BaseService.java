package ru.t1.aayakovlev.tm.service;

import java.util.List;

public interface BaseService<T> {

    List<T> findAll();

    void save(final T e);

    void deleteAll();

}
