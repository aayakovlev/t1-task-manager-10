package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.List;

public final class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository repository;

    public ProjectServiceImpl(ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty())
            return null;
        if (description == null || description.isEmpty())
            return null;

        final Project project = new Project(name, description);
        save(project);
        return project;
    }

    @Override
    public void save(final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

}
